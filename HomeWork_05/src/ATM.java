public class ATM {
    private int atmSumma; // Сумма оставшихся денег в банкомате
    private int successWithdraw; // Максимальная сумма, разрешенная к выдаче
    private int atmMax; // Максимальный объем денег (сумма, которая может быть в банкомате)
    private int operations; // Количество проведенных операций

    public int putMoney(int cash) { // Положить деньги
        if (cash + atmSumma > atmMax) {
            cash = cash - (atmMax - atmSumma);
            atmSumma = atmMax;
            return cash;
        } else {
            atmSumma += cash;
            operations++;
            return cash;
        }
    }

    public int withdrawMoney(int cash) { // Снять деньги
        if (cash > successWithdraw || cash > atmSumma) {
            cash = 0;
        }
        atmSumma -= cash;
        operations++;
        return cash;
    }

    public int getSuccessWithdraw() {
        return successWithdraw;
    }

    public void setSuccessWithdraw(int successWithdraw) {
        this.successWithdraw = successWithdraw;
    }

    public int getAtmMax() {
        return atmMax;
    }

    public void setAtmMax(int atmMax) {
        this.atmMax = atmMax;
    }

    public int getOperations() {
        return operations;
    }

    public void setOperations(int operations) {
        this.operations = operations;
    }

    public int getAtmSumma() {
        return atmSumma;
    }

    public void setAtmSumma(int atmSumma) {
        this.atmSumma = atmSumma;
    }
}