import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ATM operation = new ATM();

        operation.setAtmSumma(900000); /* Сумма оставшихся денег в банкомате */
        operation.setSuccessWithdraw(200000); /* Максимальная сумма, разрешенная к выдаче */
        operation.setAtmMax(1000000); /* Максимальный объем денег (сумма, которая может быть в банкомате) */
        operation.setOperations(0); /* Количество проведенных операций */

        while (true) {
            System.out.println("Максимальная сумма разрешённая к выдаче: " + operation.getSuccessWithdraw() + " руб.\n" +
                    "Для получения средств нажмите 1, для пополнениея, нажмите 2.\n" +
                    "Для выхода нажмите любую другую цифру и ввод."
            );
            int i = scanner.nextInt();
            if (i == 1) {
                System.out.println("Укажите запрашиваемую сумму: ");
                int a = scanner.nextInt();
                System.out.println("Вам будет выдана сумма: " + operation.withdrawMoney(a) + "руб.");
            } else if (i == 2) {
                System.out.println("Укажите размещаемую сумму: ");
                int b = scanner.nextInt();
                if ((b + operation.getAtmSumma()) > operation.getAtmMax()) {
                    System.out.println("Превышена максимальная сумма, вам будет возвращено: " + operation.putMoney(b) + "руб.");
                } else
                    System.out.println("Вами внесена сумма в размере: " + operation.putMoney(b) + "руб.");
            } else {
                System.out.println("ВЫХОД ИЗ МЕНЮ ОПЕРАЦИЙ!\n" + "PS Итоги для удобства оценки работы кода.\n" +
                        "Операций выполнено: " + operation.getOperations() + ".\n" +
                        "Сумма оставшихся денег в банкомате: " + operation.getAtmSumma() + " руб.\n" +
                        "Максимально возможная сумма в банкомате: " + operation.getAtmMax() + " руб.\n");
                break;
            }
        }

    }
}
