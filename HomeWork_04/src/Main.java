import java.util.Arrays;
import java.util.Scanner;
public class Main {
    public static int sumInRange(int theNumber, int from, int to) { // сумма цифр в диапозоне числа
        int sum = 0, i = 0;
        theNumber = Math.abs(theNumber);

        if (from > to) {
            return -1;
        }
        while (theNumber > 0) {
            i++;
            if (i >= from && i <= to) {
                sum += theNumber%10;
            }
            theNumber /= 10;
        }
        return sum;
    }
    public static void isEven(int[] array) { // поиск чётных чисел в массиве

        for(int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.println("индекс: " + i + ", его число число: " + array[i]);
            }
        }
    }
    public static int toInt(int[] number) { /* массив преобразуем в число. Ваш, Илья, вариант
    "String.valueOf(Math.abs(number)).length()". Отчасти воспользовался подсказкой мат.функции abs. */
        int result = 0;

        for (int i =number.length -1 , n = 0; i >= 0; --i, n++) {
            int pos = (int)Math.pow(10, i);
            result += Math.abs(number[n] * pos);
        }
        return result;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Код приветствует преподавателей и тренеров Иннополис!");
        /* ДЗ. ПУНКТ 1.  */
        System.out.println("ДЗ. ПУЕКТ 1.");
        System.out.println("Введите число от -2147483647 до 2147483647: ");
        int theNumber = scanner.nextInt();
        System.out.println("Введите номер первой цифры интервала: ");
        int startRange = scanner.nextInt();
        System.out.println("Введите номер второй цифры интервала: ");
        int stopRange = scanner.nextInt();
        System.out.print("Сумма чисел в указанном интервале: ");
        int sum = sumInRange(theNumber, startRange, stopRange);
        System.out.println(sum);

        /* ДЗ. ПУНКТ 2.  */
        System.out.println("ДЗ. ПУЕКТ 2.");
        System.out.println("Введите длину массива: ");
        int length = scanner.nextInt();
        int[] array = new int[length];
        System.out.println("Введите последовательность из " + length + " целых чисел.");
        for (int i = 0; i < length; i++) {
            System.out.print("Введите " + (i + 1) + " число: ");
            array[i] = scanner.nextInt();
        }
        System.out.println("Чётные числа вашего массива: ");
        isEven(array);

        /* ДЗ. ПУНКТ 3.  */
        System.out.println("ДЗ. ПУЕКТ 3.");
        System.out.println("Введите длину массива: ");
        int extent = scanner.nextInt();
        int[] number = new int[extent];
        System.out.println("Введите последовательность из " + extent + " целых чисел.");
        for (int i = 0; i < extent; i++) {
            System.out.print("Введите " + (i + 1) + " число: ");
            number[i] = scanner.nextInt();
        }
        System.out.println("Ваш массив: " + Arrays.toString(number) + " .");
        int result = toInt(number);
        System.out.println("Ваше число: " + result);
    }
}