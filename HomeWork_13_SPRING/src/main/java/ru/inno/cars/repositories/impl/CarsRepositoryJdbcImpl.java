package ru.inno.cars.repositories.impl;

import org.springframework.stereotype.Component;
import ru.inno.cars.models.Car;
import ru.inno.cars.repositories.CarsRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

//@RequiredArgsConstructor
@Component
public class CarsRepositoryJdbcImpl implements CarsRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from cars order by id";
    //language=SQL
    private static final String SQL_INSERT = "insert into cars(model, color, number, price) values (?, ?, ?, ?)";
    private static final Function<ResultSet, Car> carRowMapper = row -> {
        try {
            return Car.builder()
                    .id(row.getLong("id"))
                    .model(row.getString("model"))
                    .color(row.getString("color"))
                    .number(row.getString("number"))
                    .price(row.getDouble("price"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };
    private final DataSource dataSource;

    public CarsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car car = carRowMapper.apply(resultSet);
                    cars.add(car);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return cars;
    }

    @Override
    public void save(Car car) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT)) {
            preparedStatement.setString(1, car.getModel());
            preparedStatement.setString(2, car.getColor());
            preparedStatement.setString(3, car.getNumber());
            preparedStatement.setDouble(4, car.getPrice());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}