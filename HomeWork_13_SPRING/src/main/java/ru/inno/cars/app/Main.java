package ru.inno.cars.app;

import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repositories.CarsRepository;
import ru.inno.cars.repositories.impl.CarsRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String action = args[0];
        Scanner scanner = new Scanner(System.in);
        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader
                    (new InputStreamReader(Main.class.getResourceAsStream("/application.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));
        CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);
        if (action.equals("read")) {
            List<Car> cars = carsRepository.findAll();
            for (Car car : cars) {
                System.out.println(car);
            }
        } else if (action.equals("write")) {
            while (true) {
                System.out.println("УКАЖИТЕ МОДЕЛЬ МАШИНЫ: ");
                String model = scanner.nextLine();
                System.out.println("УКАЖИТЕ ЕЁ ЦВЕТ: ");
                String color = scanner.nextLine();
                System.out.println("УКАЖИТЕ ЕЁ НОМЕР: ");
                String number = scanner.nextLine();
                System.out.println("УКАЖИТЕ ЕЁ СТОИМОСТЬ (форма записи Double \"ххххххх.хх\": ");
                Double price = Double.valueOf(scanner.nextLine());
                Car car = Car.builder()
                        .model(model)
                        .color(number)
                        .number(color)
                        .price(price)
                        .build();
                carsRepository.save(car);
                String field = scanner.nextLine();
                if (field.equalsIgnoreCase("exit") || field.equalsIgnoreCase("-1")) {
                    return;
                }

            }
        }
    }
}
