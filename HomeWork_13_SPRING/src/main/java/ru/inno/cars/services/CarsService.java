package ru.inno.cars.services;
public interface CarsService {
    void signUp(String model, String color, String number, Double price);
}
