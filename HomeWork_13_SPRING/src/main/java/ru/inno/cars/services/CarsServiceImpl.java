package ru.inno.cars.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.cars.models.Car;
import ru.inno.cars.repositories.CarsRepository;
@Service
@RequiredArgsConstructor
public class CarsServiceImpl implements CarsService {
    private final CarsRepository carsRepository;
    @Override
    public void signUp(String model, String color, String number, Double price) {
        Car car = Car.builder()
                .model(model)
                .color(color)
                .number(number)
                .price(price)
                .build();
        carsRepository.save(car);
    }
}