package ru.inno.cars;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.inno.cars.config.AppConfig;
import ru.inno.cars.models.Car;
import ru.inno.cars.repositories.CarsRepository;
import ru.inno.cars.repositories.impl.CarsRepositoryJdbcImpl;
import ru.inno.cars.services.CarsService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        CarsService carsService = context.getBean(CarsService.class);
        CarsRepositoryJdbcImpl carsRepositoryJdbc = context.getBean(CarsRepositoryJdbcImpl.class);
        carsService.signUp("Hyundai_Palisade", "black", "а576ру778", 5280541.95);
    }
    Scanner scanner = new Scanner(System.in);
    Properties dbProperties = new Properties();
    HikariDataSource dataSource = new HikariDataSource();
    CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);
    if (context.choice.equals("read")) {
        List<Car> cars = carsRepository.findAll();
        for (Car car : cars) {
            System.out.println(car);
        }
    } else if (action.equals("write")) {
        while (true) {
            System.out.println("УКАЖИТЕ МОДЕЛЬ МАШИНЫ: ");
            String model = scanner.nextLine();
            System.out.println("УКАЖИТЕ ЕЁ ЦВЕТ: ");
            String color = scanner.nextLine();
            System.out.println("УКАЖИТЕ ЕЁ НОМЕР: ");
            String number = scanner.nextLine();
            System.out.println("УКАЖИТЕ ЕЁ СТОИМОСТЬ (форма записи Double \"ххххххх.хх\": ");
            Double price = Double.valueOf(scanner.nextLine());
            Car car = Car.builder()
                    .model(model)
                    .color(number)
                    .number(color)
                    .price(price)
                    .build();
            carsRepository.save(car);
            String field = scanner.nextLine();
            if (field.equalsIgnoreCase("exit") || field.equalsIgnoreCase("-1")) {
                return;
            }

        }
    }
}
