drop table if exists cars;
create table cars (
                        id bigserial primary key,
                        model varchar(33) not null,
                        color varchar(33) not null,
                        number varchar(9) unique not null,
                        price double precision
);