import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
//        String str = "Hello Hello Bye Hello Bye Inno";
//        Map<String, Integer> map = new HashMap<>();

//        List<String> strings = Arrays.stream("Hello Hello Bye Hello Bye Inno".split(" ")).toList();
        String[] strings = "Hello Hello Bye Hello Bye Inno".split(" "); /* Создаём строку strings и split-ом разбиваем её
                                                                                 на слова по символу "пробел". Тем самым
                                                                                 получаем последовательность значений strings */
        Map<String, Integer> map = new HashMap<>(); /* Создаём переменную map класса Map<K, V>, выделяем под неё память,
                                                    изначально она ни чего не содержит */

        for (String str : strings) { /* Создаём цикл работающий пока есть ссылки str на String-овые ключи в strings,
                                        которые были получены split-ом при создании String-овой последовательности.
                                        Тем самым перебираем слова, используемые в качестве ключей, а в значение (value)
                                        записываем количество повторений этого слова. Появляется последовательность пар,
                                        содержащих слово, как ключ (key) и сколько раз оно встретилось, как значение (value) */
            int count = map.getOrDefault(str, 0) + 1; /* По слову (ключу) достаём значение и увеличиваем его на 1,
                                                                   тем самым присваиваем значение по ключу, или изменяя,
                                                                   если слово повторилось. */
            map.put(str, count); /* Создаём пару String-ового ключа (key) и её значение (value) count, где count уже
                                    обновлён или записан первый раз с 1. */
        }
        // Эквивалентный цикл для понимания:
        // for (i = 0; i < strings.size(); ++i) {
        //    String str = strings[i];
        //    map.put(str, count);
        // }
        Map.Entry<String, Integer> maxEntry = null; /* изначально пару, которая содержит максимальное число
                                                       в значении (value), обозначаем как "ничто" (null). */
        for (Map.Entry<String, Integer> entry : map.entrySet()) { /* Создаём цикл работающий пока есть ввод пар
                                                                  entry. Map читает пары. */
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) { /* если максимальное значение ничего не
                                                                                содержит или введённое значение (value)
                                                                                пары больше ранее введённого максимального
                                                                                значения (value) содержащей его пары, */
                maxEntry = entry; /* то парой с максимальным значением становится введённая пара */
            }
        }
        System.out.println("Наиболее повторяющееся слово \"" + maxEntry.getKey() +
                                                            "\", количество повторений " + maxEntry.getValue() + ".");
    }
}