package realworld;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class UsersRepositoryFileBasedImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, User> stringToUserMapper = currentUser -> {
        String[] parts = currentUser.split("\\|");
        String firstName = parts[0];
        String lastName = parts[1];
        Integer age = Integer.parseInt(parts[2]);
        return new User(firstName, lastName, age);
    };

    private static final Function<User, String> userToStringMapper = user ->
            user.getFirstName() + "|" + user.getLastName() + "|" + user.getAge();

    // реализация с finally
//    @Override
//    public List<User> findAll() {
//        // создаем пустой список
//        List<User> users = new ArrayList<>();
//        Reader fileReader = null;
//        BufferedReader bufferedReader = null;
//        try {
//            // открывает поток для чтения из файла
//            fileReader = new FileReader(fileName);
//            // открываем буферизированный поток для быстрого чтения из файла
//            bufferedReader = new BufferedReader(fileReader);
//            // Считываем первую строку
//            String currentUser = bufferedReader.readLine();
//            // если строка ненулевая и ее получилось считать
//            while (currentUser != null) {
//                // разбиваем ее в массив через разделитель |
//                String[] parts = currentUser.split("\\|");
//                // вытаскиваем id как строку и преобразуем в число
//                Integer id = Integer.parseInt(parts[0]);
//                // имя и фамилию вытаскиваем как есть
//                String firstName = parts[1];
//                String lastName = parts[2];
//                // с возрастом делаем то же самое, что и с id
//                Integer age = Integer.parseInt(parts[3]);
//                // создаем нового пользователя
//                User user = new User(id, firstName, lastName, age);
//                // добавляем его в список
//                users.add(user);
//                // считываем новую строку
//                currentUser = bufferedReader.readLine();
//            }
//        } catch (IOException e) {
//            throw new UnsuccessfulWorkWithFileException(e);
//        } finally {
//            if (bufferedReader != null) {
//                try {
//                    bufferedReader.close();
//                } catch (IOException ignore) {}
//            }
//            if (fileReader != null) {
//                try {
//                    fileReader.close();
//                } catch (IOException ignore) {}
//            }
//        }
//        // возвращаем список пользователей
//        System.out.println(users);
//        return users;
//    }

    // реализация с try-with-resources
//    @Override
//    public List<User> findAll() {
//        List<User> users = new ArrayList<>();
//        try (Reader fileReader = new FileReader(fileName);
//             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
//            String currentUser = bufferedReader.readLine();
//            while (currentUser != null) {
//                User user = stringToUserMapper.apply(currentUser);
//                users.add(user);
//                currentUser = bufferedReader.readLine();
//            }
//        } catch (IOException e) {
//            throw new UnsuccessfulWorkWithFileException(e);
//        }
//        return users;
//    }


    // реализация через Stream API
    @Override
    public List<User> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToUserMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<User> findAllByAgeGreaterThan(Integer lower) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToUserMapper)
                    .filter(user -> user.getAge() >= lower)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public void save(User user) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String userToSave = userToStringMapper.apply(user);
            bufferedWriter.write(userToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<String> findAllUniqueNames() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToUserMapper)
                    .map(User::getFirstName) // a -> a.method() -> A::method
                    .distinct()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public int findMinAge() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToUserMapper)
                    .mapToInt(User::getAge)
                    .min()
                    .getAsInt();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public double findAverageAge() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToUserMapper)
                    .mapToInt(User::getAge)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public User findFirstWithMinAge() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToUserMapper)
                    .min(Comparator.comparingInt(User::getAge)) // (a, b) -> a.getAge() - b.getAge()
                    .get();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}