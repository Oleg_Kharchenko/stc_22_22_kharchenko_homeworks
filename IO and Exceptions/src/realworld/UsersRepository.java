package realworld;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();
    List<User> findAllByAgeGreaterThan(Integer age);

    void save(User user);

    List<String> findAllUniqueNames();

    int findMinAge();

    double findAverageAge();

    User findFirstWithMinAge();
}