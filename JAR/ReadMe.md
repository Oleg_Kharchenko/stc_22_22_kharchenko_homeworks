## I. СОЗДАНИЕ И ЗАПУСК JAVA-ФАЙЛА .JAR БЕЗ СТОРОННИХ БИБЛИОТЕК (сборка проекта, получение результирующего JAR)
### 1. Скомпилировать.
### 2. Перенести папку с ресурсами resources в папку target
### 3. Собрать(упаковать в) Java-архив .jar, предварительно перейдя в папку target и задав файл манифеста
#### ПЕРЕХОД В ПАПКУ ПРОЕКТА
```
cd ~/Documents/Java/stc_22_22_kharchenko_homeworks/JAR
```
#### ПЕРЕХОД НА ПАПКУ ВЫШЕ В ДЕРЕВЕ ПАПОК
```
cd ../
```
#### КОМПИЛЯЦИЯ ПАКЕТА И JAVA-ФАЙЛОВ .java В ПАПКУ target
```
javac -d target/ src/java/ru/inno/util/*/*.java
```
* компилировать файл можно и со сторонними библиотеками, к примеру, библиотеки находятся в папке JAR/lib/
```
javac -cp lib/jcommander-1.82.jar -d target/ src/java/ru/inno/util/*/*.java
```
#### КОПИРОВАНИЕ ПАПКИ С РЕСУРСАМИ resources В ПАКУ target
```
cp -r src/resources/ target/
```
#### ЗАПУСК ИСПОЛНИТЕЛЬНОГО ФАЙЛА MAIN
```
java ru/inno/util/app/Program
```
#### УПАКОВКА(СБОРКА) ПАКЕТА В JAVA-АРХИВ JAR
* с файлом манифеста (необходим для запуска файда .jar, как исполнительного файла)
```
jar -cvmf ../src/manifest.txt app.jar .
```
* без файла манифеста
```
jar -cvf app.jar .
```
#### ПРОСМОТР СОДЕРЖИМОГО JAVA-АРХИВА JAR
```
jar tf app.jar
```
#### РАСПАКОВАТЬ JAVA-АРХИВА JAR
```
jar xf app.jar
```
#### ЗАПУСК JAVA-АРХИВА JAR, КАК ИСПОЛНИТЕЛЬНОГО ФАЙЛА
* чтобы файл работал, необходим файл manifest.txt, который содержит путь к исполнительному файлу:
```
Main-class: ru.inno.util.app.Program
// наличие  последующего "ввода строки" обязательно.
```
```
java -jar app.jar
```
* запуск java-архива .jar с параметрами
```
java -jar app.jar --files=file1,file2,file3
```
## II. СОЗДАНИЕ И ЗАПУСК JAVA-ФАЙЛА .JAR СО СТОРОННИМИ БИБЛИОТЕКАМИ (работа с зависимостями)
### 1. Скомпилировать.
```
cd jar
javac -cp lib/jcommander-1.82.jar -d target/ src/java/ru/inno/util/*/*.java
```
### 2. Перенести папку с ресурсами resources в папку target
```
cp -r src/resources/ target/
```
### 3. Перейти в папку распаковать java-файл .jar
```
cd ../
cd lib/
jar xf jcommander-1.82.jar
cd ../
cd target/
```
### 4. Перенести папку /lib/com/ в папку target/ , находясь в папке target
```
cp -r ../lib/com/ com
```
### 5. Собрать(упаковать в) Java-архив .jar, предварительно перейдя в папку target и задав файл манифеста
```
jar -cvmf ../src/manifest.txt app.jar .
```
### 6. Запустить Java-архив .jar, как исполнительный файл
```
java -jar app.jar --files=file1,file2,file3
```
## III. MAVEN - инструмент сборки, который решает две предыдущих задачи в автосатизированном виде.

### Основное понятие MAVEN это MAVEN-проект, к примеру, с названием SimpleConsoleApplication.
Он состоит из 3 характеристик, так же называемый координатами:
1. version - версия проекта;
2. groupID - название компании-разработчика, к примеру, ru.inno, com.spring;
3. artifactID - идентификатор проекта, как правило пишется в следующей форме simple-console-application.

#### Структура MAVEN-проекта:
	* src - исходный код
		main - основной исходный
			java - исходный код на Java
			resources - файлы ресурсов
	* target - результат сборки
	* pom.xml - описание проекта

#### Жизненный цикл (Lifecycle) состоит из фаз phases:
* clean - очистка, удаление папки target
* compile - копирование ресурсов resources и компеляция в папку target

*Эти две предыдущие команды команды можно вызывать вместе.*

* package - сборка в JAR-архив. При вызове package команда compile вызовется автоматически, то бишь при нажатии package компиляция происходит автоматически. Но при этом не происходит очистка паки target. Если требуется очистка папки target, то её перед compile или package нужно сделать вручную.
