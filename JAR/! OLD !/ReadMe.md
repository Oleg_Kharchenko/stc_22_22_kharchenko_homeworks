## 1. Скомпилировать.
## 2. Перенести папку с ресурсами resources в папку target
## 3. Собрать(упаковать в) Java-архив .jar, предварительно перейдя в папку target и задав файл манифеста
## 4. 

#### ПЕРЕХОД В ПАПКУ ПРОЕКТА

```
cd ~/Documents/Java/stc_22_22_kharchenko_homeworks/JAR
```

#### ПЕРЕХОД НА ПАПКУ ВЫШЕ В ДЕРЕВЕ ПАПОК

```
cd ../
```

#### КОМПИЛЯЦИЯ ПАКЕТА И JAVA-ФАЙЛОВ .java В ПАПКУ target

```
javac -d target/ src/java/ru/inno/util/*/*.java
```

#### КОПИРОВАНИЕ ПАПКИ С РЕСУРСАМИ resources В ПАКУ target

```
cp -r src/resources/ target/
```

#### ЗАПУСК ИСПОЛНИТЕЛЬНОГО ФАЙЛА MAIN

```
java ru/inno/util/app/Program
```

#### УПАКОВКА(СБОРКА) ПАКЕТА В JAVA-АРХИВ JAR

* с файлом манифеста (необходим для запуска файда .jar, как исполнительного файла)

```
jar -cvmf ../src/manifest.txt app.jar .
```

* без файла манифеста

```
jar -cvf app.jar .
```

#### ПРОСМОТР СОДЕРЖИМОГО JAVA-АРХИВА JAR

```
jar tf app.jar
```

#### РАСПАКОВАТЬ JAVA-АРХИВА JAR

```
jar xf app.jar
```

#### ЗАПУСК JAVA-АРХИВА JAR, КАК ИСПОЛНИТЕЛЬНОГО ФАЙЛА

* чтобы файл работал, необходим файл manifest.txt, который содержит путь к исполнительному файлу:

```
Main-class: ru.inno.util.app.Program
// наличие  последующего "ввода строки" обязательно.
```

```
java -jar app.jar
```