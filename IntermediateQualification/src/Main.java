import java.io.IOException;

public class Main {
    public static void main(String[] args) {
//        try (FileReader reader = new FileReader("src\\Product.txt")){
        try {
            ProductsRepository productRepository = new ProductsRepositoryFileBasedImpl("src\\Product.txt");
            System.out.println(productRepository.findById(1));
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println(productRepository.findAllByTitleLike("оло"));
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            Product milk = productRepository.findById(1);
            System.out.println(milk);
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println("Новая стоимость молока 1000.0");
            milk.setPrice(1000.0);
            System.out.println(milk);
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println("Устанавливаем молока 10, а стоимость молока 40.1");
            milk.setCount(10);
            milk.setPrice(40.1);
            System.out.println(milk);
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println("Проверка на несуществующую запись номер 19: " + productRepository.findById(19));
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        } catch (IOException e) {
            System.out.println("Проверьте наличие файла Product.txt и форматированных данных в нём.");
        }
    }
}