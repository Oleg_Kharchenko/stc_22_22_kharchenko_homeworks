public class Ellipse extends Circle {
    protected double k;
    protected double semiAxisA = radius / 2;
    protected double semiAxisB = radius;

    public Ellipse(String name, int x, int y, int radius, double k) {
        super(name, x, y, radius);
        this.k = k;
    }

    public double area() {
        return semiAxisA * semiAxisB * Math.PI;
    }

    public double perimeter() {
        return 2 * Math.PI * Math.sqrt((Math.pow(semiAxisA, 2) + Math.pow(semiAxisB, 2)) / 2);
    }
}
