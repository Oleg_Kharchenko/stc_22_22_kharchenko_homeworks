public class Rectangle extends Square {
    protected final int sideB;

    public Rectangle(String name, int x, int y, int sideA, int sideB) {
        super(name, x, y, sideA);
        this.sideB = sideB;
    }

    public double area() {
        return sideA * sideB;
    }

    public double perimeter() {
        return 2 * sideA + 2 * sideB;
    }
}
