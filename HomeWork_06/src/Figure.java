public abstract class Figure {
    protected String name;
    protected int x;
    protected int y;

    public Figure(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public abstract double area();

    public abstract double perimeter();

    protected void move(int toX, int toY) {
        this.x = toX;
        this.y = toY;
    }
}
