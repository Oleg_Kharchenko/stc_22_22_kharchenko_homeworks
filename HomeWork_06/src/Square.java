public class Square extends Figure {
    public int sideA;

    public Square(String name, int x, int y, int sideA) {
        super(name, x, y);
        this.sideA = sideA;
    }

    public double area() {
        return sideA * sideA;
    }

    public double perimeter() {
        return 4 * sideA;
    }
}
