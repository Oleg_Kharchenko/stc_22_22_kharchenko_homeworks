public class Main {
    public static void main(String[] args) {
        // пункт 1
        Circle cir1 = new Circle("Cr1", 11, -9, 21);
        Square sq1 = new Square("Sqr1", 11, -9, 21);
        Ellipse el1 = new Ellipse("Ell1", 11, -9, 21, 0.75);
        Rectangle rt1 = new Rectangle("RcT1", 11, -9, 21, 10);
        // пункт 2
        System.out.println(cir1.name + " " + cir1.x + " " + cir1.y + " " + cir1.radius + " " + cir1.area() + " " +
                cir1.perimeter());
        System.out.println(sq1.name + " " + sq1.x + " " + sq1.y + " " + sq1.sideA + " " + sq1.area() + " " +
                sq1.perimeter());
        System.out.println(el1.name + " " + el1.x + " " + el1.y + " " + el1.k + " " + el1.area() + " " +
                el1.perimeter());
        System.out.println(rt1.name + " " + rt1.x + " " + rt1.y + " " + sq1.sideA + " " + rt1.sideB + " " + rt1.area() +
                " " + rt1.perimeter());
        // пункт 3
        cir1.move(19,-7);
        System.out.println(cir1.name + " " + cir1.x + " " + cir1.y + " " + cir1.radius + " " + cir1.area() + " " +
                cir1.perimeter());
        sq1.move(19,-7);
        System.out.println(sq1.name + " " + sq1.x + " " + sq1.y + " " + sq1.sideA + " " + sq1.area() + " " +
                sq1.perimeter());
        el1.move(19,-7);
        System.out.println(el1.name + " " + el1.x + " " + el1.y + " " + el1.k + " " + el1.area() + " " +
                el1.perimeter());
        rt1.move(19,-7);
        System.out.println(rt1.name + " " + rt1.x + " " + rt1.y + " " + sq1.sideA + " " + rt1.sideB + " " + rt1.area() +
                " " + rt1.perimeter());
    }
}
