# JDBC
### JDBC (Java DataBase Connectivity — соединение с базами данных на Java) - стандарт взаимодействия Java-приложений с различными СУБД, реализованный в виде пакетов java.sql и javax.sql, входящих в состав Java SE.
Данный стандарт описан специфкицией "JSR 221 JDBC 4.1 API". Спецификация рассказывает нам о том, что JDBC API предоставляет программный доступ к реляционным базам данных из программ, написанных на Java. Так же рассказывает о том, что JDBC API является частью платформы Java и входит поэтому в Java SE и Java EE. JDBC API представлен двумя пакетами: java.sql and javax.sql.

    * Java Application
        JDBC API (содержит интерфейсы Connection, Statement, ResultSet. У этих интерфейсов отсутствует реализация. В качестве реализации используется драйвер.)
            JDBC Driver Manager (Драйверы для интерфейсов предоставляют реализацию PgConnection, PgStatement, PgResultSet.
                JDBC Driver <<=>> Oracle
                JDBC Driver <<=>> SQL Serve
                JDBC Driver <<=>> ODBC Data Source
                JDBC Driver <<=>> PostgreSQL
                JDBC Driver <<=>> MySQL
                JDBC Driver <<=>> H2 Database
                ....

