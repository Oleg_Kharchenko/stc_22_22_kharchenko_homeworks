package ru.inno.education.app;

import com.zaxxer.hikari.HikariDataSource;
import ru.inno.education.models.Student;
import ru.inno.education.repositories.StudentsRepository;
import ru.inno.education.repositories.StudentsRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);

        Student student = Student.builder()
                .password("qwerty111")
                .firstName("Павел")
                .lastName("Александров")
                .isWorker(true)
                .email("pavel@gmail.com")
                .average(5.0)
                .age(34)
                .build();

        System.out.println(student);
        studentsRepository.save(student);
        System.out.println(student);

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
}
