public class Main {
    public static void main(String[] args) {
        ArrayTask sumFromToOfArray = new SumFromToOfArray();
        ArrayTask sumOfDigitFromArray = new SumOfDigitFromArray();
        ArraysTasksResolver tasksResolver = new ArraysTasksResolver();
        int[] array = {12, 62, 4, 2, 100, 40, 56};
        int from = 1, to = 3;

        ArrayTask task1 = (x, y, z) -> {
            int count = 0;
            int sum1 = 0;
            for (count = y; count <= z; count++) {
                sum1 += x[count];
            }
            return sum1;
        };
        int result1 = task1.resolve(array, from, to);
        System.out.println(result1);

        ArrayTask task2 = (x, y, z) -> {
            int count = 0;
            int sum2 = 0;
            int maxValue = 0;
            for (count = y; count <= z; count++) {
                if (x[count] > maxValue) {
                    maxValue = x[count];
                }
            }
            while (maxValue != 0) {
                sum2 += (maxValue % 10);
                maxValue /= 10;
            }
            return sum2;
        };
        int result2 = task2.resolve(array, from, to);
        System.out.println(result2);

        ArrayTask task3 = (x, y, z) -> {
            ArrayTask sumFromToOfArray(array, from, to);
            return sumFromToOfArray;
        };

        int asd = 0;
    }
}
