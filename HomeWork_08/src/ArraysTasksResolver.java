public class ArraysTasksResolver {
    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        task.resolve(array, from, to);
        resolveTask(array, task, from, to);
    }
}
