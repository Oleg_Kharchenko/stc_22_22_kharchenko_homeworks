package ru.inno;
import java.util.ListIterator;
public class Main {
    public static String mergeDocuments(Iterable<String> documents) {
        StringBuilder mergedDocument = new StringBuilder();
        Iterator<String> documentsIterator = documents.iterator();
        while (documentsIterator.hasNext()) {
            mergedDocument.append(documentsIterator.next() + " ");
        }
        return mergedDocument.toString();
    }
    public static void main(String[] args) {
        List<String> stringList = new LinkedList<>();
        stringList.add("Hello!");
        stringList.add("Bye!");
        stringList.add("Fine!");
        stringList.add("C++!");
        stringList.add("PHP!");
        stringList.add("Cobol!");
        List<Integer> integerList = new LinkedList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        String documents = mergeDocuments(stringList);
//        mergeDocuments(integerList);
        System.out.println(documents);

    }
}