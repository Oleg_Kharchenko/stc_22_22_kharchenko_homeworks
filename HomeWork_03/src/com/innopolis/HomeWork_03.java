package com.innopolis;

import java.util.Arrays;
import java.util.Scanner;

public class HomeWork_03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Код приветствует преподавателей и тренеров Иннополис!\nВведите длину массива: ");

        int length = scanner.nextInt();

        int[] array = new int[length];
        int[] arr2 = new int[length + 2];

        int j, left, current, right, max = Integer.MAX_VALUE;

        j = 0;
        arr2[0] = max;
        arr2[length+1] = max;

        System.out.println("Введите последовательность из " + length + " целых чисел.");

        for (int i = 0; i < length; i++) {
            System.out.print("Введите " + (i + 1) + " число: ");
            array[i] = scanner.nextInt();
            arr2[i+1] = array[i];
        }

        if (length > 1) {

            for (int tact = -1; tact < arr2.length - 3; tact++) {

                left = arr2[(tact + 1)];
                current = arr2[(tact + 2)];
                right = arr2[(tact + 3)];

                if (left > current && current < right) {

                    System.out.println("Число " + current + " является локальным минимумом.");
                    j++;
                }
            }

        } else if (length == 1) {
            System.out.println("В массиве всего одно число!");
        }

        System.out.println("Ваш масив данных из " + length + " чисел состоит из " + Arrays.toString(array) + " .");

        if (j > 0) {

            System.out.println("Локальных минимумов в данном массиве " + j + ".");

        } else {

            System.out.println("В массиве нет локальных минимумов!");

        }

    }
}
