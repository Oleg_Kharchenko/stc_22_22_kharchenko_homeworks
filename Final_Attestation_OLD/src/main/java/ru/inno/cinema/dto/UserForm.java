package ru.inno.cinema.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserForm {
    @Size(min = 2, max = 20)
    private String firstName;

    @NotNull
    @NotBlank
    private String lastName;

    @Email
    private String email;
    private String password;

    private Integer age;

}
