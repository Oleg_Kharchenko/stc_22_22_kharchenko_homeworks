package ru.inno.cinema.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.cinema.models.Course;
import ru.inno.cinema.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByCoursesNotContainsAndState(Course course, User.State state);

    List<User> findAllByCoursesContains(Course course);

    Optional<User> findByEmail(String email);
}
