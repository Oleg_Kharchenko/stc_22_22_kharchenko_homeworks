package ru.inno.cinema.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.cinema.models.Course;

public interface CoursesRepository extends JpaRepository<Course, Long> {
}
