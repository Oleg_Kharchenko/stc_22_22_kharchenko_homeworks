package ru.inno.cinema.services;

import ru.inno.cinema.dto.UserForm;

public interface SignUpService {
    void signUp(UserForm userForm);
}
