package ru.inno.cinema.services;

import ru.inno.cinema.models.Course;
import ru.inno.cinema.models.User;

import java.util.List;

public interface CoursesService {
    void addStudentToCourse(Long courseId, Long studentId);

    Course getCourse(Long courseId);

    List<User> getNotInCourseStudents(Long courseId);

    List<User> getInCourseStudents(Long courseId);
}
