package ru.inno.cinema.services;

import ru.inno.cinema.models.User;
import ru.inno.cinema.security.details.CustomUserDetails;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);
}
