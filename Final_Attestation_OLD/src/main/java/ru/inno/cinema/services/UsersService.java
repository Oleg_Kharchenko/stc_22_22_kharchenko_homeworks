package ru.inno.cinema.services;

import ru.inno.cinema.dto.UserForm;
import ru.inno.cinema.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);
}
