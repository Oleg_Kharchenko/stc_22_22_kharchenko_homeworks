package ru.inno.cinema.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.cinema.models.User;
import ru.inno.cinema.repositories.UsersRepository;
import ru.inno.cinema.security.details.CustomUserDetails;
import ru.inno.cinema.services.ProfileService;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final UsersRepository usersRepository;

    @Override
    public User getCurrent(CustomUserDetails userDetails) {
        return usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
    }
}
