insert into visitor (email, password, age, is_ticketed)
values ('marsel@gmail.com', 'qwerty007', 28, true);
insert into visitor (email, password, age, is_ticketed)
values ('stanislav@mail.ru', 'qwerty008', 38, false);
insert into visitor (email, password, age, is_ticketed)
values ('yurii@yandex.ru', 'qwerty009', 27, true);
insert into visitor (email, password, age, is_ticketed)
values ('vladislav@yahoo.com', 'qwerty010', 20, false);

update visitor
set first_name = 'Марсель',
    last_name  = 'Сидиков'
where id = 1;
update visitor
set first_name = 'Станислав',
    last_name  = 'Жербеков'
where id = 2;
update visitor
set first_name = 'Юрий',
    last_name  = 'Вертелецкий'
where id = 3;
update visitor
set first_name = 'Владислав',
    last_name  = 'Малышев'
where id = 4;

insert into session (title, description, start, finish)
values ('10 декабря', 'Детский сеанс', '2022-12-10 09:00', '2022-12-10 16:00');
insert into session (title, description, start, finish)
values ('10 декабря', 'Ретроспектива', '2022-12-10 09:00', '2022-12-10 16:00');
insert into session (title, description, start, finish)
values ('10 декабря', 'Закрытый показ', '2022-12-10 22:00', '2022-12-11 01:00');
insert into session (title, description, start, finish)
values ('10 декабря', 'Общий показ', '2022-12-10 13:00', '2022-12-10 23:00');
insert into session (title, description, start, finish)
values ('11 декабря', 'Детский сеанс', '2022-12-11 09:00', '2022-12-11 16:00');
insert into session (title, description, start, finish)
values ('11 декабря', 'Ретроспектива', '2022-12-11 09:00', '2022-12-11 16:00');
insert into session (title, description, start, finish)
values ('11 декабря', 'Закрытый показ', '2022-12-11 22:00', '2022-12-12 01:00');
insert into session (title, description, start, finish)
values ('11 декабря', 'Общий показ', '2022-12-11 13:00', '2022-12-11 23:00');

insert into film (name, summary, start_time, finish_time, session_id)
values ('Чёрная Пантера: Ваканда навеки', 'Африканская сверхдержава сопротивляется новым врагам. Cиквел одного из самых кассовых фильмов марвеловской франшизы', '19:20', '20:54', 4),
       ('Чёрная Пантера: Ваканда навеки', 'Африканская сверхдержава сопротивляется новым врагам. Cиквел одного из самых кассовых фильмов марвеловской франшизы', '20:20', '21:54', 4),
       ('Чёрная Пантера: Ваканда навеки', 'Африканская сверхдержава сопротивляется новым врагам. Cиквел одного из самых кассовых фильмов марвеловской франшизы', '21:25', '23:59', 4),
       ('Чёрная Пантера: Ваканда навеки', 'Африканская сверхдержава сопротивляется новым врагам. Cиквел одного из самых кассовых фильмов марвеловской франшизы', '22:25', '23:59', 4),
       ('Чёрная Пантера: Ваканда навеки', 'Африканская сверхдержава сопротивляется новым врагам. Cиквел одного из самых кассовых фильмов марвеловской франшизы', '19:20', '20:54', 8),
       ('Чёрная Пантера: Ваканда навеки', 'Африканская сверхдержава сопротивляется новым врагам. Cиквел одного из самых кассовых фильмов марвеловской франшизы', '20:20', '21:54', 8),
       ('Чёрная Пантера: Ваканда навеки', 'Африканская сверхдержава сопротивляется новым врагам. Cиквел одного из самых кассовых фильмов марвеловской франшизы', '21:25', '23:59', 8),
       ('Чёрная Пантера: Ваканда навеки', 'Африканская сверхдержава сопротивляется новым врагам. Cиквел одного из самых кассовых фильмов марвеловской франшизы', '22:25', '23:59', 8);

insert into film (name, summary, start_time, finish_time, session_id)
values ('Не дыши: Начало', 'Стивен Лэнг берёт на прицел очередного незваного гостя', '22:00', '23:57', 3);
insert into film (name, summary, start_time, finish_time, session_id)
values ('Не дыши: Начало', 'Стивен Лэнг берёт на прицел очередного незваного гостя', '22:00', '23:57', 7);

insert into audience_session (visitor_id, session_id)
values (1, 1),
       (1, 2),
       (2, 3),
       (4, 3),
       (1, 3),
       (2, 4)
