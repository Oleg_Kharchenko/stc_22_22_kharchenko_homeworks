drop table if exists audience_session;
drop table if exists visitor;
drop table film;
drop table if exists session;
drop table if exists persistent_logins;

create table visitor
(
    id         bigserial primary key, -- первичный ключ (большое целое), генерируется СУБД
    email      varchar(20) unique, -- уникальное строковое значение с максимальной длиной 20
    password   varchar(100),
    first_name varchar(20) default 'DEFAULT_FIRSTNAME', -- строковое значение со значением по умолчанию
    last_name  varchar(20) default 'DEFAULT_LASTNAME',
    age        integer check (age >= 0 and age <= 120) not null, -- числовое значение, с проверкой значения, не допускается null
    is_ticketed  bool -- логическое значение
);

alter table visitor
    add column average double precision check ( average >= 0 and average <= 5) default 3,
    add column state varchar(13) default 'CONFIRMED',
    add column role varchar(5) default 'USER';

create table session
(
    id          serial primary key,
    title       char(10) not null,
    description char(500),
    start       timestamp,
    finish      timestamp
);

create table film
(
    id          serial primary key,
    name        char(55),
    summary     char(1000),
    start_time  time,
    finish_time time,
    session_id   integer not null,
    foreign key (session_id) references session (id) -- внешний ключ, course_id ссылается на id таблицы course
);

create table audience_session (
                                visitor_id bigint,
                                session_id bigint,
                                foreign key (visitor_id) references visitor (id),
                                foreign key (session_id) references session(id)
);

alter table film
    alter column session_id drop not null;

create table if not exists persistent_logins
(
    username  varchar(64) not null,
    series    varchar(64) not null,
    token     varchar(64) not null,
    last_used timestamp   not null
);
