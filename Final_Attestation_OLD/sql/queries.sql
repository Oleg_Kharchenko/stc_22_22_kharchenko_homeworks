-- получение всех посетителей
select *
from visitor;
-- получение только имени и фамилии посетителей
select first_name, last_name
from visitor;
-- получение посетителей, упорядоченных по возрасту (если возраст совпал - по id)
select *
from visitor
order by age, id;
-- получение посетителей, упорядоченных по id в обратном порядке
select *
from visitor
order by id desc;
-- получить все уникальные возраста
select distinct(age)
from visitor;
-- получить все возраста и сколько раз они встречаются
select age, count(*)
from visitor
group by age;

-- получение посетителей, у которых возраст больше 25 лет
select *
from visitor
where age > 25;
-- получение количества посетителей, у которых возраст меньше 24 лет
select count(*)
from visitor
where age < 24;
-- получение посетителей, которые работают или старше 30 лет
select *
from visitor
where age > 30
   or is_ticketed = true;
-- получение посетителей, у которых в имени есть "ов"
select *
from visitor
where "like"(last_name, '%ов');
-- получение сеансов, которые начинаются в 2022-м и заканчиваются после 2023
select *
from session
where (start between '2022-01-01' and '2022-12-31')
  and (finish > '2023-12-31');

-- получить название тех сеансов, у которых есть фильмы, начинающиеся в 10 часов
select title
from session
where id in (select distinct(session_id)
             from film
             where start_time = '10:00:00');
-- получить имена посетителей, у которых фильмы начнутся в 10:00
select first_name, last_name
from visitor
where id in (select distinct(visitor_id)
             from audience_session
             where session_id in (select distinct (session_id)
                                 from film
                                 where start_time = '10:00:00'));

-- получаем фильмы и их сеансы (только те фильмы, у которых есть сеансы, и сеансы, у которых есть фильмы)
-- (inner) join
select *
from film l
         join session c on c.id = l.session_id
order by l.id;

-- получаем фильмы и их сеансы (получаем все фильмы и сеансы, у которых есть фильмы)
-- left (outer) join
select *
from film l
         left join session c on c.id = l.session_id
order by l.id;

-- получаем фильмы и их сеансы (получаем только те фильмы, у которых есть сеансы, и все сеансы)
-- right (outer) join
select *
from film l
         right join session c on c.id = l.session_id
order by l.id;

-- получаем фильмы и их сеансы (получаем все фильмы и сеансы)
-- full (outer) join
select *
from film l
         full join session c on c.id = l.session_id
order by l.id;
