import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число начала диапазона и число окончания диапазона (числа введите через ВВОД): ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        Task[] tasks = new Task[]{
                new EvenNumbersPrintTask(a, b),
                new OddNumbersPrintTask(a, b)
        };
        completeAllTasks(tasks);

        int w = 0;
    }
    private static void completeAllTasks(Task[] tasks) {
        for (Task task : tasks) {
            task.complete();
        }
    }
}