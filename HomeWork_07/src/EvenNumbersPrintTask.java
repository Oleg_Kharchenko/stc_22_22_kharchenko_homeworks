public class EvenNumbersPrintTask extends AbstractNumbersPrintTask {
    public EvenNumbersPrintTask(int from, int to) {
        super(from, to);
    }
    int j = 0;
    @Override
    public void complete() {
        for (int i = from; i < to; i++) {
            if (i % 2 == 0) {
                System.out.print(i + ", ");
                j++;
            }
        }
        System.out.println("чётных чисел в диапазоне " + j + ".");
    }
}
