public class OddNumbersPrintTask extends AbstractNumbersPrintTask {
    public OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }
    int j = 0;
    @Override
    public void complete() {
        for (int i = from; i < to; i++) {
            if (i % 2 == 1 || i % 2 == -1) {
                System.out.print(i + ", ");
                j++;
            }
        }
        System.out.println("нечётных чисел в диапазоне " + j + ".");
    }
}