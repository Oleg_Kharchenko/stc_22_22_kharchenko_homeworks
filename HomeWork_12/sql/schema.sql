drop table if exists driver, car, trip;
create table driver (
                        id bigserial primary key,
                        first_name varchar(33) not null,
                        last_name varchar(33) not null,
                        phone_number varchar(12) unique not null,
                        experience integer check (experience >= 0 and experience <= 102) not null,
                        age integer check (age >= 16 and age <= 120) not null,
                        license bool not null,
                        lic_cat varchar(11),
                        rating integer
);
create table car (
                     id bigserial primary key,
                     model varchar(21) not null,
                     color varchar(13) not null,
                     number varchar(9) unique not null,
                     id_driver bigint not null,
                     foreign key (id_driver) references driver(id)
);
create table trip (
                      id bigserial primary key,
                      id_driver bigint not null,
                      foreign key (id_driver) references driver(id),
                      id_car bigint not null,
                      foreign key (id_car) references car(id),
                      date_trip timestamp,
                      duration time
);