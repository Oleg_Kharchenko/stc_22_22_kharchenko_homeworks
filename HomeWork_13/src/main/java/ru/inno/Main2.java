package ru.inno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.inno.services.CarsServiceImpl;

public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        CarsServiceImpl carsService = context.getBean("carsService", CarsServiceImpl.class);
        carsService.signUp("Hyundai_Palisade", "black", "а576ру777", 5280541.95);
    }
}
