package ru.inno.services;
import lombok.RequiredArgsConstructor;
import ru.inno.models.Car;
import ru.inno.repository.CarsRepository;

@RequiredArgsConstructor
public class CarsServiceImpl {
    private final CarsRepository carsRepository;
    public void signUp(String model, String color, String number, Double price) {
        Car car = Car.builder()
                .model(model)
                .color(color)
                .number(number)
                .price(price)
                .build();
        carsRepository.save(car);
    }
}