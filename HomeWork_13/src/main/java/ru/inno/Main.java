package ru.inno;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.repository.impl.CarsRepositoryFilesImpl;
import ru.inno.repository.impl.CarsRepositoryJdbcImpl;
import ru.inno.services.CarsServiceImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
public class Main {
    public static void main(String[] args) {
        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/application.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));
        CarsRepositoryFilesImpl carsRepositoryFiles = new CarsRepositoryFilesImpl("cars.txt");
        CarsRepositoryJdbcImpl usersRepositoryJdbc = new CarsRepositoryJdbcImpl(dataSource);
        CarsServiceImpl carsService = new CarsServiceImpl(usersRepositoryJdbc);
        carsService.signUp("Hyundai_Palisade", "black", "а576ру777", 5280541.95);
    }
}