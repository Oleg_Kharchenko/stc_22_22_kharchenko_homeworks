package ru.inno.repository;

import ru.inno.models.Car;

public interface CarsRepository {
    void save(Car car);
}
