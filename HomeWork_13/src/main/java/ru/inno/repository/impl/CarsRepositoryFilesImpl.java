package ru.inno.repository.impl;

import ru.inno.models.Car;
import ru.inno.repository.CarsRepository;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CarsRepositoryFilesImpl implements CarsRepository {
    private final String fileName;

    public CarsRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(Car car) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String carToSave = car.getModel() + "|" + car.getColor() + "|" + car.getNumber() + "|" + car.getPrice();
            bufferedWriter.write(carToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
